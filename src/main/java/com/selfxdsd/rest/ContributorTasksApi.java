/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.Contract;
import com.selfxdsd.api.Task;
import com.selfxdsd.api.User;
import com.selfxdsd.rest.output.JsonTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Tasks assigned to the authenticated User, in a certain Contract.
 * @author criske
 * @version $Id$
 * @since 0.0.1
 */
@RestController
@RequestMapping("/contracts/{owner}/{name}/tasks/{role}")
public class ContributorTasksApi extends BaseRestController {

    /**
     * Authenticated user.
     */
    private final User user;

    /**
     * Constructor.
     * @param user Authenticated User.
     */
    @Autowired
    public ContributorTasksApi(final User user) {
        this.user = user;
    }

    /**
     * Get a single Contract Task of the authenticated User.<br><br>
     *
     * @return Task or 204 NO CONTENT if not found.
     * @param owner Login of the user or organization who owns the repo.
     * @param name Repo name.
     * @param role Role of the contributor in contract.
     * @param issueId Issue ID from Github, Gitlab etc.
     * @param isPullRequest Is it a Pull Request or an Issue?
     * @checkstyle ParameterNumber (10 lines).
     */
    @GetMapping(
        value = "/{issueId}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> task(
        @PathVariable final String owner,
        @PathVariable final String name,
        @PathVariable final String role,
        @PathVariable final String issueId,
        @RequestParam("isPullRequest") final boolean isPullRequest
    ){
        final ResponseEntity<String> resp;
        final Contract.Id id = new Contract.Id(
            owner + "/" + name,
            this.user.username(),
            this.user.provider().name(),
            role
        );
        final Task task = this.user.asContributor().tasks().ofContract(id)
            .getById(
                issueId,
                id.getRepoFullName(),
                id.getProvider(),
                isPullRequest
            );
        if (task == null) {
            resp = ResponseEntity.noContent().build();
        } else {
            resp = ResponseEntity.ok(new JsonTask(task).toString());
        }
        return resp;
    }
}