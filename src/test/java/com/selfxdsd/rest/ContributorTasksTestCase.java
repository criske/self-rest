/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.*;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.StringReader;
import java.util.Objects;

/**
 * Unit tests for {@link ContributorTasksApi}.
 * @author criske
 * @version $Id$
 * @since 0.0.1
 */
public final class ContributorTasksTestCase {

    /**
     * ContributorTaskApi#task(...) can return a JSON Task of authenticated
     * User Contract.
     * @checkstyle ExecutableStatementCount (110 lines).
     */
    @Test
    public void returnsTaskOk() {
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Tasks contributorTasks = Mockito.mock(Tasks.class);
        final Tasks contractTasks = Mockito.mock(Tasks.class);
        final Task task = Mockito.mock(Task.class);
        final Project project = Mockito.mock(Project.class);
        final Provider provider = Mockito.mock(Provider.class);
        final Contract.Id contractId = new Contract.Id(
            "mihai/test",
            "amihaiemil",
            Provider.Names.GITHUB,
            Contract.Roles.DEV
        );

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);
        Mockito.when(contributor.tasks()).thenReturn(contributorTasks);
        Mockito.when(contributorTasks.ofContract(contractId))
            .thenReturn(contractTasks);
        Mockito.when(contractTasks.getById(
            "#1",
            "mihai/test",
            Provider.Names.GITHUB,
            false
        )).thenReturn(task);
        Mockito.when(task.issueId()).thenReturn("#1");
        Mockito.when(task.role()).thenReturn(Contract.Roles.DEV);
        Mockito.when(task.isPullRequest()).thenReturn(false);
        Mockito.when(task.estimation()).thenReturn(60);
        Mockito.when(task.project()).thenReturn(project);
        Mockito.when(project.repoFullName()).thenReturn("mihai/test");
        Mockito.when(project.provider()).thenReturn(Provider.Names.GITHUB);

        final ContributorTasksApi api = new ContributorTasksApi(user);
        final ResponseEntity<String> resp = api
            .task("mihai", "test", Contract.Roles.DEV, "#1", false);
        final JsonObject jsonTask = Json.createReader(
            new StringReader(Objects.requireNonNull(resp.getBody()))
        ).readObject();
        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.OK)
        );
        MatcherAssert.assertThat(
            jsonTask,
            Matchers.equalTo(
                Json.createObjectBuilder()
                    .add("issueId", "#1")
                    .add("isPullRequest", task.isPullRequest())
                    .add("role", Contract.Roles.DEV)
                    .add("repoFullName", "mihai/test")
                    .add("provider", Provider.Names.GITHUB)
                    .add("estimation", "60 min.")
                    .build()
            )
        );
    }


    /**
     * ContributorTaskApi#task(...) can return NO_CONTENT if a Task of the
     * authenticated User Contract is not found.
     */
    @Test
    public void returnsTaskNoContent() {
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Tasks contributorTasks = Mockito.mock(Tasks.class);
        final Tasks contractTasks = Mockito.mock(Tasks.class);
        final Provider provider = Mockito.mock(Provider.class);
        final Contract.Id contractId = new Contract.Id(
            "mihai/test",
            "amihaiemil",
            Provider.Names.GITHUB,
            Contract.Roles.DEV
        );

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);
        Mockito.when(contributor.tasks()).thenReturn(contributorTasks);
        Mockito.when(contributorTasks.ofContract(contractId))
            .thenReturn(contractTasks);

        final ContributorTasksApi api = new ContributorTasksApi(user);
        final ResponseEntity<String> resp = api
            .task("mihai", "test", Contract.Roles.DEV, "#1", false);

        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.NO_CONTENT)
        );
    }
}