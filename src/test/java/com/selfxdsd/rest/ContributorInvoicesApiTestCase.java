/**
 * Copyright (c) 2020-2021, Self XDSD Contributors
 * All rights reserved.
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"),
 * to read the Software only. Permission is hereby NOT GRANTED to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software.
 * <p>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.selfxdsd.rest;

import com.selfxdsd.api.*;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.StringReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Unit tests for {@link ContributorInvoicesApi}.
 * @author criske
 * @version $Id$
 * @since 0.0.1
 */
public final class ContributorInvoicesApiTestCase {

    /**
     * ContributorInvoicesApi#invoice(...) can return OK if a Invoice of the
     * authenticated User Contract is found.
     * @checkstyle ExecutableStatementCount (110 lines).
     */
    @Test
    public void returnsInvoiceOk() {
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Provider provider = Mockito.mock(Provider.class);
        final Contract contract = Mockito.mock(Contract.class);
        final Invoices invoices = Mockito.mock(Invoices.class);
        final Invoice invoice = Mockito.mock(Invoice.class);

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);
        Mockito.when(contributor.contract(
            "mihai/test",
            Provider.Names.GITHUB, Contract.Roles.DEV)
        ).thenReturn(contract);
        Mockito.when(contract.invoices()).thenReturn(invoices);
        Mockito.when(contract.contractId()).thenReturn(
            new Contract.Id(
                "mihai/test",
                "amihaiemil",
                "github",
                "DEV"
            )
        );
        Mockito.when(invoice.invoiceId()).thenReturn(1);
        Mockito.when(invoice.createdAt()).thenReturn(LocalDateTime
            .of(2021, 2, 8, 0, 0, 0));
        Mockito.when(invoice.billedBy()).thenReturn("XD Tech S.R.L.");
        Mockito.when(invoice.billedTo()).thenReturn("Mihai Andronache");
        Mockito.when(invoice.commission()).thenReturn(BigDecimal.valueOf(100));
        Mockito.when(invoice.totalAmount()).thenReturn(BigDecimal.valueOf(119));
        Mockito.when(invoice.isPaid()).thenReturn(false);
        Mockito.when(invoice.contract()).thenReturn(contract);
        Mockito.when(invoices.getById(1)).thenReturn(invoice);

        final ContributorInvoicesApi api = new ContributorInvoicesApi(user);
        final ResponseEntity<String> resp = api
            .invoice("mihai", "test", Contract.Roles.DEV, 1);
        final JsonObject jsonInvoice = Json.createReader(
            new StringReader(Objects.requireNonNull(resp.getBody()))
        ).readObject();
        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.OK)
        );
        MatcherAssert.assertThat(
            jsonInvoice.getInt("id"),
            Matchers.equalTo(1)
        );
    }

    /**
     * ContributorInvoicesApi#invoice(...) can return NO_CONTENT if a Invoice
     * of the authenticated User Contract is not found.
     */
    @Test
    public void returnsInvoiceNoContent() {
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Provider provider = Mockito.mock(Provider.class);
        final Contract contract = Mockito.mock(Contract.class);
        final Invoices invoices = Mockito.mock(Invoices.class);

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);
        Mockito.when(contributor.contract(
            "mihai/test",
            Provider.Names.GITHUB, Contract.Roles.DEV)
        ).thenReturn(contract);
        Mockito.when(contract.invoices()).thenReturn(invoices);
        Mockito.when(contract.contractId()).thenReturn(
            new Contract.Id(
                "mihai/test",
                "amihaiemil",
                "github",
                "DEV"
            )
        );

        final ContributorInvoicesApi api = new ContributorInvoicesApi(user);
        final ResponseEntity<String> resp = api
            .invoice("mihai", "test", Contract.Roles.DEV, 1);

        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.NO_CONTENT)
        );
    }

    /**
     * ContributorInvoicesApi#invoice(...) can return NO_CONTENT if
     * authenticated User Contract is not found.
     */
    @Test
    public void returnsInvoiceNoContentNoContract() {
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Provider provider = Mockito.mock(Provider.class);

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);

        final ContributorInvoicesApi api = new ContributorInvoicesApi(user);
        final ResponseEntity<String> resp = api
            .invoice("mihai", "test", Contract.Roles.DEV, 1);

        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.NO_CONTENT)
        );
    }

    /**
     * ContributorInvoicesApi#all(...) can return OK the
     * authenticated User Contract is found.
     * @checkstyle ExecutableStatementCount (110 lines).
     */
    @Test
    public void returnsAllInvoicesOk(){
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Provider provider = Mockito.mock(Provider.class);
        final Contract contract = Mockito.mock(Contract.class);
        final Invoices invoices = Mockito.mock(Invoices.class);
        final Invoice invoice = Mockito.mock(Invoice.class);

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);
        Mockito.when(contributor.contract(
            "mihai/test",
            Provider.Names.GITHUB, Contract.Roles.DEV)
        ).thenReturn(contract);
        Mockito.when(contract.invoices()).thenReturn(invoices);
        Mockito.when(contract.contractId()).thenReturn(
            new Contract.Id(
                "mihai/test",
                "amihaiemil",
                "github",
                "DEV"
            )
        );
        Mockito.when(invoice.invoiceId()).thenReturn(1);
        Mockito.when(invoice.createdAt()).thenReturn(LocalDateTime
            .of(2021, 2, 8, 0, 0, 0));
        Mockito.when(invoice.billedBy()).thenReturn("XD Tech S.R.L.");
        Mockito.when(invoice.billedTo()).thenReturn("Mihai Andronache");
        Mockito.when(invoice.commission()).thenReturn(BigDecimal.valueOf(100));
        Mockito.when(invoice.totalAmount()).thenReturn(BigDecimal.valueOf(119));
        Mockito.when(invoice.isPaid()).thenReturn(false);
        Mockito.when(invoice.contract()).thenReturn(contract);
        Mockito.when(invoices.spliterator())
            .thenReturn(List.of(invoice).spliterator());

        final ContributorInvoicesApi api = new ContributorInvoicesApi(user);
        final ResponseEntity<String> resp = api
            .all("mihai", "test", Contract.Roles.DEV);
        final JsonArray jsonInvoices = Json.createReader(
            new StringReader(Objects.requireNonNull(resp.getBody()))
        ).readArray();
        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.OK)
        );
        MatcherAssert.assertThat(
            jsonInvoices.getJsonObject(0).getInt("id"),
            Matchers.equalTo(1)
        );
    }

    /**
     * ContributorInvoicesApi#all(...) can return NO_CONTENT if
     * authenticated User Contract is not found.
     */
    @Test
    public void returnsAllNoContentNoContract() {
        final User user = Mockito.mock(User.class);
        final Contributor contributor = Mockito.mock(Contributor.class);
        final Provider provider = Mockito.mock(Provider.class);

        Mockito.when(provider.name()).thenReturn(Provider.Names.GITHUB);
        Mockito.when(user.asContributor()).thenReturn(contributor);
        Mockito.when(user.username()).thenReturn("amihaiemil");
        Mockito.when(user.provider()).thenReturn(provider);

        final ContributorInvoicesApi api = new ContributorInvoicesApi(user);
        final ResponseEntity<String> resp = api
            .all("mihai", "test", Contract.Roles.DEV);

        MatcherAssert.assertThat(
            resp.getStatusCode(),
            Matchers.equalTo(HttpStatus.NO_CONTENT)
        );
    }
}