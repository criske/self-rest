<img alt="self-xsdsd-logo" src="https://self-xdsd.com/files/self-xdsd.png" width="80" height="80"/>

[![Coverage Status](https://coveralls.io/repos/gitlab/self-xdsd/self-rest/badge.svg?branch=master)](https://coveralls.io/gitlab/self-xdsd/self-rest?branch=master)

# Self Rest

RESTful API of Self XDSD.

## Contributing 

If you would like to contribute, just open an issue or a Merge Request.

You will need Java 11.
Make sure the maven build:

``$mvn clean install -Pcheckstyle,itcases``

passes before opening a Merge Request. [Checkstyle](http://checkstyle.sourceforge.net/) will make sure
you're following our code style and guidelines.

It's better to make changes on a separate branch (derived from ``master``), so you won't have to cherry pick commits in case your PR is rejected.

## LICENSE

This product's code is open source. However, the [LICENSE](https://gitlab.com/self-xdsd/self-rest/blob/master/LICENSE) only allows you to read the code. Copying, downloading or forking the repo is strictly forbidden unless you are one of the project's contributors.
